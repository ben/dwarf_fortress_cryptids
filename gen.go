package main

import (
	"os"
	"os/exec"
	"strings"

	"github.com/davecgh/go-spew/spew"
)

func genRaws() ([][]string, error) {
	if err := os.RemoveAll("data/save/region1"); err != nil {
		return nil, err
	}

	cmd := exec.Command("./df", "-gen", "1", "RANDOM", "SMALL REGION")
	cmd.Env = append(cmd.Env, "TERM=xterm")
	if b, err := cmd.CombinedOutput(); err != nil {
		spew.Dump(b)
		return nil, err
	}

	f, err := os.Open("data/save/region1/world.dat")
	if err != nil {
		return nil, err
	}

	raws, err := readCreatureRaws(f)
	if err != nil {
		_ = f.Close()
		return nil, err
	}

	return raws, f.Close()
}

func genMessages() ([]string, error) {
	raws, err := genRaws()
	if err != nil {
		return nil, err
	}

	messages := make([]string, 0, len(raws))
	for _, creature := range raws {
		var name, description string
		var spheres []string
		for _, tag := range creature {
			if strings.HasPrefix(tag, "[NAME:") {
				name = capitalize(strings.Split(tag, ":")[1])
			}
			if strings.HasPrefix(tag, "[DESCRIPTION:") {
				description = strings.TrimSuffix(strings.TrimPrefix(tag, "[DESCRIPTION:"), "]")
			}
			if strings.HasPrefix(tag, "[SPHERE:") {
				sphere := strings.ToLower(strings.TrimSuffix(strings.TrimPrefix(tag, "[SPHERE:"), "]"))
				if replacement, ok := replaceSpheres[sphere]; ok {
					sphere = replacement
				}
				spheres = append(spheres, sphere)
			}
		}

		message := "Dwarf Fortress Cryptid: " + name + "\n\n" + description
		if len(spheres) != 0 {
			message += "\n\nAssociated with " + makeList(spheres) + "."
		}
		messages = append(messages, message)
	}
	return messages, nil
}

func capitalize(s string) string {
	s = strings.Title(s)
	s = strings.ReplaceAll(s, "'S ", "'s ")
	s = strings.ReplaceAll(s, " Of ", " of ")
	s = strings.ReplaceAll(s, " The ", " the ")
	return s
}

var replaceSpheres = map[string]string{
	"moon":  "the moon",
	"night": "the night",
	// use a slightly less anxiety-provoking word
	"suicide": "struggle",
}

func makeList(s []string) string {
	if len(s) == 1 {
		return s[0]
	}

	if len(s) == 2 {
		return s[0] + " and " + s[1]
	}

	if len(s) > 2 {
		s[len(s)-1] = "and " + s[len(s)-1]
	}

	return strings.Join(s, ", ")
}
