module git.lubar.me/ben/dwarf_fortress_cryptids

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/uuid v1.1.1
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
)
