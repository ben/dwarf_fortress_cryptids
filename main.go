package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"time"

	"github.com/google/uuid"
	"github.com/tomnomnom/linkheader"
)

var rng = rand.New(rand.NewSource(time.Now().UnixNano()))

var (
	flagServer       = flag.String("server", "https://botsin.space", "")
	flagAccessToken  = flag.String("access-token", "", "")
	flagTootInterval = flag.Duration("toot-interval", 3*time.Hour, "")
)

func main() {
	flag.Parse()
	if *flagAccessToken == "" {
		log.Fatal("-access-token is required")
	}

	cryptids := make(chan string, 1)
	go generateToots(cryptids)

	postEvery := *flagTootInterval

	initialSleep := postEvery - time.Since(time.Now().Truncate(postEvery))
	log.Println("sleeping", initialSleep, "to line up with", postEvery, "boundary")
	time.Sleep(initialSleep)

	for range time.Tick(postEvery) {
		postToot(<-cryptids)
	}
}

func generateToots(cryptids chan<- string) {
	for {
		log.Println("Generating cryptids...")
		start := time.Now()
		messages, err := genMessages()
		if err != nil {
			panic(err)
		}
		log.Println("Generated", len(messages), "cryptids in", time.Since(start))

		rng.Shuffle(len(messages), func(i, j int) {
			messages[i], messages[j] = messages[j], messages[i]
		})

		for _, m := range messages {
			cryptids <- m
		}
	}
}

func postToot(message string) {
	timeLimit := time.Now().Add(15 * time.Minute)

	post := []byte(url.Values{
		"status":     {message},
		"visibility": {"unlisted"},
		"language":   {"en"},
	}.Encode())

	idempotency, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}

	backoff := time.Second / 2
	for {
		time.Sleep(backoff)
		req, err := http.NewRequest("POST", *flagServer+"/api/v1/statuses", bytes.NewReader(post))
		if err != nil {
			panic(err)
		}
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		req.Header.Set("Idempotency-Key", idempotency.String())
		if _, err = apiRequest(req, nil); err != nil {
			log.Println("error posting status:", err)
			if time.Now().Add(10*time.Minute + backoff).After(timeLimit) {
				log.Println("too late - dropping status:", message)
				break
			}
			backoff *= 2
			log.Println("waiting", backoff, "and trying again")
		} else {
			log.Println("posted status:", message)
			break
		}
	}
}

func apiRequest(req *http.Request, data interface{}) (*http.Response, error) {
	req.Header.Set("Authorization", "Bearer "+*flagAccessToken)
	req.Header.Set("User-Agent", "DwarfFortressCryptids/1.0 (+https://git.lubar.me/ben/dwarf_fortress_cryptids)")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusTooManyRequests {
		return resp, errors.New("server returned " + resp.Status)
	}

	if resp.StatusCode == http.StatusUnprocessableEntity {
		b, _ := ioutil.ReadAll(resp.Body)
		return resp, errors.New("server returned " + resp.Status + ": " + string(b))
	}

	if resp.StatusCode >= 400 {
		panic(req.Method + " " + req.URL.RequestURI() + ": server returned " + resp.Status)
	}

	if data != nil {
		return resp, json.NewDecoder(resp.Body).Decode(data)
	}

	return resp, resp.Body.Close()
}

func pagedApiRequest(uri string, data interface{}) error {
	allData := reflect.ValueOf(data).Elem()
	for {
		pageData := reflect.New(allData.Type())
		req, err := http.NewRequest("GET", uri, nil)
		if err != nil {
			return err
		}
		resp, err := apiRequest(req, pageData.Interface())
		if err != nil {
			return err
		}
		allData.Set(reflect.AppendSlice(allData, pageData.Elem()))
		nextLink := linkheader.ParseMultiple(resp.Header["Link"]).FilterByRel("next")
		if len(nextLink) == 0 {
			return nil
		}
		uri = nextLink[0].URL
	}
}
