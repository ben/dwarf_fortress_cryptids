package main

import (
	"compress/zlib"
	"encoding/binary"
	"errors"
	"io"
	"io/ioutil"
)

type chunkyZlibReader struct {
	r     io.Reader
	chunk []byte
	err   error
}

func (r *chunkyZlibReader) Read(b []byte) (int, error) {
	if r.err != nil {
		return 0, r.err
	}

	if len(r.chunk) != 0 || len(b) == 0 {
		n := copy(b, r.chunk)
		r.chunk = r.chunk[n:]
		return n, nil
	}

	var length uint32
	if r.err = binary.Read(r.r, binary.LittleEndian, &length); r.err != nil {
		return 0, r.err
	}

	rc, err := zlib.NewReader(io.LimitReader(r.r, int64(length)))
	if err != nil {
		r.err = err
		return 0, err
	}
	r.chunk, err = ioutil.ReadAll(rc)
	if err != nil {
		_ = rc.Close()
		r.err = err
		return 0, err
	}
	if err = rc.Close(); err != nil {
		r.err = err
		return 0, err
	}

	n := copy(b, r.chunk)
	r.chunk = r.chunk[n:]
	return n, nil
}

func skip(r io.Reader, n int) error {
	if n < 0 {
		return errors.New("negative length")
	}
	_, err := io.ReadFull(r, make([]byte, n))
	return err
}

func skipString(r io.Reader) error {
	var length int16
	if err := binary.Read(r, binary.LittleEndian, &length); err != nil {
		return err
	}
	return skip(r, int(length))
}

func skipList(r io.Reader, f func(io.Reader) error) error {
	var length int32
	if err := binary.Read(r, binary.LittleEndian, &length); err != nil {
		return err
	}
	if length < 0 {
		return errors.New("negative length")
	}
	for i, l := 0, int(length); i < l; i++ {
		if err := f(r); err != nil {
			return err
		}
	}
	return nil
}

// code page 437 as used by Dwarf Fortress
var cp437 = []rune("\x00☺☻♥♦♣♠•◘○◙♂♀♪♬☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼ !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~⌂ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜ¢£¥₧ƒáíóúñÑªº¿⌐¬½¼¡«»░▒▓│┤╡╢╖╕╣║╗╝╜╛┐└┴┬├─┼╞╟╚╔╩╦╠═╬╧╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀αßΓπΣσµτΦΘΩδ∞φε∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²■\u00A0")

func readString(r io.Reader) (string, error) {
	var length uint16
	if err := binary.Read(r, binary.LittleEndian, &length); err != nil {
		return "", err
	}

	bs := make([]byte, length)
	if _, err := io.ReadFull(r, bs); err != nil {
		return "", err
	}

	rs := make([]rune, length)
	for i, b := range bs {
		rs[i] = cp437[b]
	}

	return string(rs), nil
}
