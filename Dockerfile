FROM golang:1.15 AS build

COPY . /src

RUN cd /src \
 && CGO_ENABLED=0 go build -o /dwarf_fortress_cryptids

FROM benlubar/dwarffortress:df-0.47.04

COPY --from=build /dwarf_fortress_cryptids /usr/local/bin/

RUN sed -e 's/END_YEAR:1050/END_YEAR:2/' -i /df_linux/data/init/world_gen.txt

ENTRYPOINT ["dwarf_fortress_cryptids"]

CMD []
