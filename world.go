package main

import (
	"encoding/binary"
	"io"
)

func readCreatureRaws(r io.Reader) ([][]string, error) {
	var header struct {
		// internal save file version number
		Version uint32
		// 0 => uncompressed, 1 => chunky zlib
		CompressionType uint32
	}
	if err := binary.Read(r, binary.LittleEndian, &header); err != nil {
		return nil, err
	}

	switch header.CompressionType {
	case 0:
		// uncompressed
	case 1:
		r = &chunkyZlibReader{r: r}
	}

	if err := skip(r, binary.Size(struct {
		_ uint16     // unused
		_ [37]uint32 // "next ID" fields

		// world name
		_ uint8     // presence (always 1)
		_ uint16    // given name of world (always 0 bytes in length)
		_ uint16    // nickname of world (always 0 bytes in length)
		_ [7]uint32 // name word indices
		_ [7]uint16 // name word parts of speech
		_ uint32    // world name language
		_ uint16    // naming mode

		_ [15]byte // unknown
	}{})); err != nil {
		return nil, err
	}

	if err := skipString(r); err != nil {
		return nil, err
	}

	// three kinds of raws before creature
	for i := 0; i < 3; i++ {
		if err := skipList(r, func(r io.Reader) error {
			return skipList(r, skipString)
		}); err != nil {
			return nil, err
		}
	}

	var length int32
	if err := binary.Read(r, binary.LittleEndian, &length); err != nil {
		return nil, err
	}
	raws := make([][]string, length)
	for i := range raws {
		if err := binary.Read(r, binary.LittleEndian, &length); err != nil {
			return nil, err
		}
		raw := make([]string, length)
		for j := range raw {
			s, err := readString(r)
			if err != nil {
				return nil, err
			}
			raw[j] = s
		}
		raws[i] = raw
	}
	return raws, nil
}
